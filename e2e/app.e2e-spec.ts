import { BuilditFrontEndPage } from './app.po';

describe('buildit-front-end App', () => {
  let page: BuilditFrontEndPage;

  beforeEach(() => {
    page = new BuilditFrontEndPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
