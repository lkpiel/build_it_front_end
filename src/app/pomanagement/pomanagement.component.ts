import { Component, OnInit } from '@angular/core';
import { PurchaseOrderService } from "app/pomanagement/purchaseorder.service";

@Component({
  selector: 'app-pomanagement',
  templateUrl: './pomanagement.component.html',
  styleUrls: ['./pomanagement.component.css']
})
export class PomanagementComponent  {
  constructor(private purchaseOrderService: PurchaseOrderService) {}

  ngOnInit(){
    //do any lines of code to init the child
    this.purchaseOrderService.getPurchaseOrders();
  }

}
