import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PomanagementComponent } from './pomanagement.component';

describe('PomanagementComponent', () => {
  let component: PomanagementComponent;
  let fixture: ComponentFixture<PomanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PomanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PomanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
