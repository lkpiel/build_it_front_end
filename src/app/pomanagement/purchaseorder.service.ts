import { Injectable, Input } from "@angular/core";
import { Http } from "@angular/http";
import { PurchaseOrder } from "app/definitions";


@Injectable()
export class PurchaseOrderService {
    purchaseOrders: Array<PurchaseOrder> = [];
    constructor(private http: Http) {}

    getPurchaseOrders() {
        this.http
            .get('http://localhost:8080/api/procurements/orders')
            .subscribe(response => this.purchaseOrders = response.json());
    }        
   
}