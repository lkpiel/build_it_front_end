import { Component, OnInit, Injectable } from '@angular/core';
import { PlantHireRequest } from "app/definitions";

@Component({
  selector: 'app-phrmanagement',
  templateUrl: './phrmanagement.component.html',
  styleUrls: ['./phrmanagement.component.css']
})
export class PhrmanagementComponent {
  isPHRDetailsTabActive = false;
  isPlantHireRequestTabActive = true;
  isPHRStatusTabActive = false;
  isPHRExtendTabActive = false;
  selectedPlantHireRequest: PlantHireRequest;

  showPHRDetailsTab(plantHireRequest: PlantHireRequest) {
    this.isPHRDetailsTabActive = true;
    this.isPlantHireRequestTabActive = false;
    this.isPHRStatusTabActive = false;
    this.isPHRExtendTabActive = false;
    this.selectedPlantHireRequest = plantHireRequest;
  }
  showPlantHireRequestTab() {
    console.log("SHOWING PLANT HIRE REQUEST TAB");
    this.isPHRDetailsTabActive = false;
    this.isPlantHireRequestTabActive = true;
    this.isPHRStatusTabActive = false;
    this.isPHRExtendTabActive = false;
  }
  showPHRStatusTab(plantHireRequest: PlantHireRequest) {
    this.isPHRDetailsTabActive = false;
    this.isPlantHireRequestTabActive = false;
    this.isPHRStatusTabActive = true;
    this.isPHRExtendTabActive = false;
    this.selectedPlantHireRequest = plantHireRequest;
  }
  showPHRExtendTab(plantHireRequest: PlantHireRequest) {
    this.isPHRDetailsTabActive = false;
    this.isPlantHireRequestTabActive = false;
    this.isPHRStatusTabActive = false;
    this.isPHRExtendTabActive = true;
    this.selectedPlantHireRequest = plantHireRequest;
    console.log("SIIA TULINGI NAH");
  }



}
