import { Injectable, Input } from "@angular/core";
import { Http } from "@angular/http";
import { Query, PlantInventoryEntry, PurchaseOrder, PlantHireRequest } from "app/definitions";

import * as moment from "moment"
import { PhrmanagementComponent } from "app/phrmanagement/phrmanagement.component";

@Injectable()
export class PlantHireRequestService {
    plantHireRequests: Array<PlantHireRequest> = [];
    constructor(private http: Http) {}

    getPlantHireRequests() {
        this.http
            .get('http://localhost:8080/api/procurements/planthirerequests')
            .subscribe(response => this.plantHireRequests = response.json());
           
    }        
   
}