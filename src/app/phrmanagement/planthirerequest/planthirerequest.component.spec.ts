import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanthirerequestComponent } from './planthirerequest.component';

describe('PlanthirerequestComponent', () => {
  let component: PlanthirerequestComponent;
  let fixture: ComponentFixture<PlanthirerequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanthirerequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanthirerequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
