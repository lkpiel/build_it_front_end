import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PlantHireRequestService } from "./planthirerequest.service";
import { PlantHireRequest } from "app/definitions";
import { Http } from "@angular/http";

@Component({
  selector: 'app-planthirerequest',
  templateUrl: './planthirerequest.component.html',
  styleUrls: ['./planthirerequest.component.css']
})
export class PlantHireRequestComponent {
  @Output() showPHRDetailsTab: EventEmitter<PlantHireRequest> = new EventEmitter(); 
  @Output() showPHRStatusTab: EventEmitter<PlantHireRequest> = new EventEmitter();
  @Output() showPHRExtendTab: EventEmitter<PlantHireRequest> = new EventEmitter();
  @Output() showPlantHireRequestTab:EventEmitter<null> = new EventEmitter();


  constructor(private http:Http, private plantHireRequestService: PlantHireRequestService) {}
  ngOnInit(){
    //do any lines of code to init the child
    this.plantHireRequestService.getPlantHireRequests();
  }
  cancelPHR(plantHireRequestID: string){
        this.http
            .post('http://localhost:8080/api/procurements/planthirerequests/' + plantHireRequestID +'/cancel', null)
            .subscribe(response => this.plantHireRequestService.getPlantHireRequests());
      
    }
}
