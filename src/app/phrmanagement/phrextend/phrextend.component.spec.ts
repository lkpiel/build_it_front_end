import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhrextendComponent } from './phrextend.component';

describe('PhrextendComponent', () => {
  let component: PhrextendComponent;
  let fixture: ComponentFixture<PhrextendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhrextendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhrextendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
