import { Component, OnInit, Input } from '@angular/core';
import { PlantHireRequest, Query, PlantInventoryEntry } from "app/definitions";
import { RequisitionService } from "app/requisition/requisition.service";

@Component({
  selector: 'app-phrextend',
  templateUrl: './phrextend.component.html',
  styleUrls: ['./phrextend.component.css']
})
export class PhrextendComponent {
  @Input() selectedPlantHireRequest: PlantHireRequest;
  query: Query = new Query();
  
  constructor(private requisitionService: RequisitionService){}
    ngOnInit(){
      this.requisitionService.plants = [];
      this.query.name = this.selectedPlantHireRequest.plantInventoryEntryDTO.name;
      this.query.startDate = this.selectedPlantHireRequest.businessPeriodDTO.endDate;
    }

    extendPHR(plantInventoryEntry: PlantInventoryEntry){
      this.requisitionService.query = this.query;
      this.requisitionService.selectPlant(plantInventoryEntry);
    }


}
