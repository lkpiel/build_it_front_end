import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhrmanagementComponent } from './phrmanagement.component';

describe('PhrmanagementComponent', () => {
  let component: PhrmanagementComponent;
  let fixture: ComponentFixture<PhrmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhrmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhrmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
