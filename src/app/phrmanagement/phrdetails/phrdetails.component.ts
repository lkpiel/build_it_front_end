import { Component, OnInit, Input } from '@angular/core';
import { PlantHireRequest, PlantInventoryEntry } from "app/definitions";
import { RequisitionService } from "app/requisition/requisition.service";

@Component({
  selector: 'app-phrdetails',
  templateUrl: './phrdetails.component.html',
  styleUrls: ['./phrdetails.component.css']
})
export class PhrdetailsComponent {
    @Input() selectedPlantHireRequest: PlantHireRequest;
    constructor(private requisitionService: RequisitionService){}
    ngOnInit(){
      this.requisitionService.plants = [];
    }
    updatePHR(plant:PlantInventoryEntry){
      this.requisitionService.updatePHR(this.selectedPlantHireRequest, plant);
    }
}
