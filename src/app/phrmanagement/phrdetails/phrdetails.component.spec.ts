import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhrdetailsComponent } from './phrdetails.component';

describe('PhrdetailsComponent', () => {
  let component: PhrdetailsComponent;
  let fixture: ComponentFixture<PhrdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhrdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhrdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
