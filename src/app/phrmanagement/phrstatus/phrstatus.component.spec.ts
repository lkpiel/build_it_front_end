import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhrstatusComponent } from './phrstatus.component';

describe('PhrstatusComponent', () => {
  let component: PhrstatusComponent;
  let fixture: ComponentFixture<PhrstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhrstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhrstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
