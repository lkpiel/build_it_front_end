import { Injectable, Input, Host, Output } from "@angular/core";
import { Http } from "@angular/http";
import { Query, PlantInventoryEntry, PurchaseOrder, PlantHireRequest } from "app/definitions";

import * as moment from "moment"
import { PhrmanagementComponent } from "app/phrmanagement/phrmanagement.component";
import { EventEmitter } from "events";
import 'rxjs/add/operator/map';


@Injectable()
export class PHRStatusService {
    constructor(private http: Http) { }
    plantHireRequest:PlantHireRequest;
    rejectPHR(plantHireRequestID: number) {
      return  this.http.delete('http://localhost:8080/api/procurements/planthirerequests/' + plantHireRequestID + '/accept' )
      .map(response =>  response.json());
    }
    acceptPHR(plantHireRequestID: number) {
        this.http.post('http://localhost:8080/api/procurements/planthirerequests/' + plantHireRequestID + '/accept', null)
            .subscribe(response => console.log(response.json()));
        console.log("ACCEPTED");
    }
}