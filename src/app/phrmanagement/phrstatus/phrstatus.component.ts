import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PlantHireRequest } from "app/definitions";
import { PHRStatusService } from "app/phrmanagement/phrstatus/phrstatus.service";
import { Http } from "@angular/http";

@Component({
  selector: 'app-phrstatus',
  templateUrl: './phrstatus.component.html',
  styleUrls: ['./phrstatus.component.css']
})
export class PhrstatusComponent {
  @Output() showPlantHireRequestTab:EventEmitter<null> = new EventEmitter();
  @Input() selectedPlantHireRequest: PlantHireRequest;
  constructor(private http:Http, private phrStatusService: PHRStatusService) {}
  rejectPHR(id:number){
    this.http.delete('http://localhost:8080/api/procurements/planthirerequests/' + id + '/accept' )
            .subscribe(response => this.showPlantHireRequestTab.emit());
  }
  acceptPHR(id:number){
    this.http.post('http://localhost:8080/api/procurements/planthirerequests/' + id.toString() + '/accept', null)
            .subscribe(response => this.showPlantHireRequestTab.emit());
  }

}
