import { Component, OnInit } from '@angular/core';
import { Invoice } from "app/definitions";

@Component({
  selector: 'app-invoicemanagement',
  templateUrl: './invoicemanagement.component.html',
  styleUrls: ['./invoicemanagement.component.css']
})
export class InvoicemanagementComponent {
  isInvoiceDetailsTabActive = false;
  isInvoiceTabActive = true;
  selectedInvoice: Invoice;
  

  showInvoiceDetailsTab(invoice: Invoice) {
    this.isInvoiceDetailsTabActive = true;
    this.isInvoiceTabActive = false;
    this.selectedInvoice = invoice;
  }
  showInvoiceTab() {
    this.isInvoiceDetailsTabActive = false;
    this.isInvoiceTabActive = true;
  }
}
