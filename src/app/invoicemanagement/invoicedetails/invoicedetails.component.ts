import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Invoice } from "app/definitions";
import { Http } from "@angular/http";
import { InvoicingService } from "app/invoicemanagement/invoice/invoicing.service";

@Component({
  selector: 'app-invoicedetails',
  templateUrl: './invoicedetails.component.html',
  styleUrls: ['./invoicedetails.component.css']
})
export class InvoicedetailsComponent{
  @Input() selectedInvoice: Invoice;
  @Output() showInvoiceTab:EventEmitter<null> = new EventEmitter();

  constructor(private http:Http, private invoicingService: InvoicingService) {}
  rejectInvoice(id:number){
    this.http.delete('http://localhost:8080/api/procurements/invoices/' + id + '/accept' )
            .subscribe(response => this.showInvoiceTab.emit());
  }
  acceptInvoice(id:number){
    this.http.post('http://localhost:8080/api/procurements/invoices/' + id.toString() + '/accept', null)
            .subscribe(response => this.showInvoiceTab.emit());
  }

}
