import { Injectable, Input } from "@angular/core";
import { Invoice } from "app/definitions";
import { Http } from "@angular/http";


@Injectable()
export class InvoicingService {
    invoices: Array<Invoice> = [];
    constructor(private http: Http) {}

    getInvoices() {
        this.http
            .get('http://localhost:8080/api/procurements/invoices')
            .subscribe(response => this.invoices = response.json());
            console.log("Käiv");
    }    
}