import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { InvoicingService } from "app/invoicemanagement/invoice/invoicing.service";
import { Invoice } from "app/definitions";

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent{
  @Output() showInvoiceDetailsTab: EventEmitter<Invoice> = new EventEmitter(); 
  constructor(private invoicingService: InvoicingService) {}
  ngOnInit(){
    //do any lines of code to init the child
    this.invoicingService.getInvoices();
  }

}
