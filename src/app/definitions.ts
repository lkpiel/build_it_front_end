export class Query {
  name: string;
  startDate: Date;
  endDate: Date;
}

export class PlantInventoryEntry {
    _id: string;
    name: string;
    description: string;
    price: number;
    _links: {[rel: string]: Relation};
    supplier: string;
}

export class Relation {
    href: string;
}

export class BusinessPeriod {
  startDate: Date;
  endDate: Date;
}

export class PurchaseOrder {
  _id: string;
  plant: PlantInventoryEntry;
  rentalPeriod: BusinessPeriod;
  total: number;
  status: POStatus;
}

export enum POStatus {
  PENDING, OPEN, REJECTED, CLOSED
}
export class Invoice {
    _id: string;
    amount: number;
    purchaseOrderDTO: PurchaseOrder;
    dueDate: Date;
    invoiceStatus: InvoiceStatus;
    plantHireRequestDTO: PlantHireRequest;
}
export class PlantHireRequest {
    _id : string;
    constructionSiteID: string;
    cost: number;
    comments: string;
    extensionEndDate: Date;
    extensionStatus: ExtensionStatus;
    siteEngineer: Employee;
    worksEngineer: Employee;
    status: PlantHireRequestStatus;
    purchaseOrder: PurchaseOrder;
    plantInventoryEntryDTO: PlantInventoryEntry;
    businessPeriodDTO: BusinessPeriod;
}
export enum ExtensionStatus{
    ACCEPTED, REJECTED, PENDING, NA
}
export enum PlantHireRequestStatus{
     ACCEPTED, REJECTED, PENDING
}
export class Employee{
    name: string;
}
export enum InvoiceStatus{
    PENDING, APPROVED, REJECTED, PAID, OVERDUE
}
