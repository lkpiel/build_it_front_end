import { Component } from '@angular/core';
import { PlantHireRequestService } from "app/phrmanagement/planthirerequest/planthirerequest.service";
import { InvoicingService } from "app/invoicemanagement/invoice/invoicing.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private plantHireRequestService: PlantHireRequestService, private invoicingService: InvoicingService) {}
  title = 'app works!';
  isRequisitionTabActive = true;
  isPHRManagementTabActive = false;
  isInvoiceManagementTabActive = false;
  isPOManagementTabActive = false;

  showInvoiceManagementTab(){
    console.log("Invoice management");
    this.isInvoiceManagementTabActive = true;
    this.isRequisitionTabActive = false;
    this.isPHRManagementTabActive = false;
    this.invoicingService.getInvoices();
    this.isPOManagementTabActive = false;

  }
  showPHRManagementTab(){
    this.isRequisitionTabActive = false;
    this.isPHRManagementTabActive = true;
    this.isInvoiceManagementTabActive = false;
    this.isPOManagementTabActive = false;
    this.plantHireRequestService.getPlantHireRequests();
  }
  showRequisitionTab(){
    this.isRequisitionTabActive = true;
    this.isPHRManagementTabActive = false;
    this.isInvoiceManagementTabActive = false;
    this.isPOManagementTabActive = false;
  }
  showPurchaseOrderTab(){
     this.isRequisitionTabActive = false;
    this.isPHRManagementTabActive = false;
    this.isInvoiceManagementTabActive = false;
    this.isPOManagementTabActive = true;
  }
}
