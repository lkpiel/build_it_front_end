import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { RequisitionComponent } from './requisition/requisition.component';
import { QueryFormComponent } from './requisition/query-form/query-form.component';
import { RequisitionService } from "app/requisition/requisition.service";
import { QueryResultComponent } from './requisition/query-result/query-result.component';
import { PlantHireRequestComponent } from './phrmanagement/planthirerequest/planthirerequest.component';
import { PlantHireRequestService } from "app/phrmanagement/planthirerequest/planthirerequest.service";
import { PhrmanagementComponent } from './phrmanagement/phrmanagement.component';
import { PhrdetailsComponent } from './phrmanagement/phrdetails/phrdetails.component';
import { PhrstatusComponent } from './phrmanagement/phrstatus/phrstatus.component';
import { PHRStatusService } from "app/phrmanagement/phrstatus/phrstatus.service";
import { PhrextendComponent } from './phrmanagement/phrextend/phrextend.component';
import { InvoicemanagementComponent } from './invoicemanagement/invoicemanagement.component';
import { InvoiceComponent } from './invoicemanagement/invoice/invoice.component';
import { InvoicingService } from "app/invoicemanagement/invoice/invoicing.service";
import { InvoicedetailsComponent } from './invoicemanagement/invoicedetails/invoicedetails.component';
import { PomanagementComponent } from './pomanagement/pomanagement.component';
import { PurchaseOrderService } from "app/pomanagement/purchaseorder.service";

@NgModule({
  declarations: [
    AppComponent,
    RequisitionComponent,
    QueryFormComponent,
    QueryResultComponent,
    PlantHireRequestComponent,
    PhrmanagementComponent,
    PhrdetailsComponent,
    PhrstatusComponent,
    PhrextendComponent,
    InvoicemanagementComponent,
    InvoiceComponent,
    InvoicedetailsComponent,
    PomanagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    RequisitionService,
    PlantHireRequestService,
    PHRStatusService,
    InvoicingService,
    PurchaseOrderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
