import { Injectable, Input } from "@angular/core";
import { Http } from "@angular/http";
import { Query, PlantInventoryEntry, PurchaseOrder, PlantHireRequest } from "app/definitions";

import * as moment from "moment"

@Injectable()
export class RequisitionService {
    query: Query;
    plants: Array<PlantInventoryEntry> = [];
    order: PurchaseOrder = new PurchaseOrder();
    

    constructor(private http: Http) {}

    executeQuery(query: Query) {
        this.query = query;
        var params = new URLSearchParams();
        this.http
            .get('http://localhost:8080/api/procurements/plants', 
            {
                params: 
                {
                    name: this.query.name,
                    startDate: this.query.startDate,
                    endDate: this.query.endDate
                }
            })
            .subscribe(response => this.plants = response.json());
    }       
    selectPlant (plantInventoryEntry: PlantInventoryEntry) {
        this.plants = [];
        console.log("SELECTE THIS PLANT PROVIDER" + plantInventoryEntry.supplier);
        this.order.plant = plantInventoryEntry;
        this.order.rentalPeriod = {startDate: this.query.startDate, endDate: this.query.endDate};
        this.order.total = (moment(this.query.endDate).diff(moment(this.query.startDate), 'days') + 1) * this.order.plant.price;
        this.http
            .post('http://localhost:8080/api/procurements/planthirerequests', this.order)
            .subscribe(response => console.log(response));
    }       
    updatePHR(selectedPlantHireRequest:PlantHireRequest,plantInventoryEntry:PlantInventoryEntry){
        this.order.plant = plantInventoryEntry;
        this.order.rentalPeriod = {startDate: this.query.startDate, endDate: this.query.endDate};
        this.order.total = (moment(this.query.endDate).diff(moment(this.query.startDate), 'days') + 1) * this.order.plant.price;
        console.log(this.order);
        this.http.put('http://localhost:8080/api/procurements/planthirerequests/' + selectedPlantHireRequest._id , this.order)
            .subscribe(response => console.log(response));
    }
}