import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Query } from "app/definitions";

@Component({
  selector: 'app-query-form',
  templateUrl: './query-form.component.html',
  styleUrls: ['./query-form.component.css']
})
export class QueryFormComponent {
  @Output() executeEventQuery: EventEmitter<Query> = new EventEmitter(); 
  query: Query = new Query();

}
