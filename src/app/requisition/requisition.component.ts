import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Query, PlantInventoryEntry, PlantHireRequest } from "app/definitions";
import { RequisitionService } from "app/requisition/requisition.service";

@Component({
  selector: 'app-requisition',
  templateUrl: './requisition.component.html',
  styleUrls: ['./requisition.component.css']
})
export class RequisitionComponent {
    query: Query;

    @Output() showPHRManagementTab: EventEmitter<null> = new EventEmitter();
    setPHRManagementVisible(){
       this.showPHRManagementTab.emit();
    }

    constructor(private requisitionService: RequisitionService) {}
    
    selectPlant(plant:PlantInventoryEntry){
      this.requisitionService.selectPlant(plant);
      this.setPHRManagementVisible();
    }
    
}
