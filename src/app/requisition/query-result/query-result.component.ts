import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { RequisitionService } from "app/requisition/requisition.service";
import { PlantInventoryEntry, PlantHireRequest } from "app/definitions";

@Component({
  selector: 'app-query-result',
  templateUrl: './query-result.component.html',
  styleUrls: ['./query-result.component.css']
})
export class QueryResultComponent {
  @Output() selectPlant: EventEmitter<PlantInventoryEntry> = new EventEmitter();

  constructor(private requisitionService: RequisitionService){}

}
